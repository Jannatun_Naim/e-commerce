<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
  </head>
  <body>

  @include('admin.sidebar')

      <!-- partial -->
      
            @include('admin.navbar')

        <!-- partial -->

        <div class="container-fluid page-body-wrapper">
            <div class="container" align="center">

                @if(session('message'))
                    <span class="text-success">{{ session('message') }}</span>
                @endif

                <table class="table ">
                    <tr style=" ">
                        <th style="padding:20px;">Title</th>
                        <th style="padding:20px;">Price</th>
                        <th style="padding:20px;">Description</th>
                        <th style="padding:20px;">Quantity</th>
                        <th style="padding:20px;">Image</th>
                        <th style="padding:20px;">Action</th>
                    </tr>

                    @foreach($data as $product)

                    <tr >
                        <td >{{$product->title}}</td>
                        <td >${{$product->price}}</td>
                        <td >{{$product->description}}</td>
                        <td >{{$product->quantity}}</td>
                        <td ><img heigt="" width="" src="/productimage/{{$product->image}}" alt=""></td>
                        <td>
                            <a href="{{url('updateview',$product->id)}}"><button type="button" class="btn btn-primary"><i class="far fa-eye">Update</i></button></a>
                            <a href="{{url('deleteproduct',$product->id)}}"><button type="button" class="btn btn-danger"><i class="far fa-trash-alt">Delete</i></button></a>
                        </td>
                    </tr>
                    @endforeach

                </table>
            </div>
        </div>

          <!-- partial -->
        
          @include('admin.script')


  </body>
</html>